# Landscape genomics of *Escherichia coli* in livestock-keeping households across a rapidly developing urban city

## Author affiliations

Dishon M. Muloi<sup>1,2,3*</sup>, Bryan A. Wee<sup>1*</sup>, **Deirdre M. H. McClean<sup>1</sup>**, Melissa E. Ward<sup>3,4</sup>, Louise Pankhurst<sup>4</sup>, Hang Phan<sup>4</sup>, Alistair Ivens<sup>3</sup>, Velma Kivali<sup>2</sup>, Alice Kiyonga<sup>2</sup>, Christine Ndinda<sup>2</sup>, Nduhiu Gitahi<sup>5</sup>, Tom Ouko<sup>6</sup>, James Hassell<sup>2,7</sup>, Titus Imboma<sup>8</sup>, James Akoko<sup>2</sup>, Maurice Karani<sup>2</sup>, Samuel Njoroge<sup>6</sup>, Patrick Muinde<sup>2</sup>, Yukiko Nakamura<sup>9</sup>, Lorren Alumasa<sup>2</sup>, Erin Furmaga<sup>10</sup>, Titus Kaitho<sup>11</sup>, Elin Öhgren<sup>12</sup>, Fredrick Amanya<sup>2</sup>, Allan Ogendo<sup>2</sup>, Daniel Wilson<sup>13</sup>, Judy Bettridge<sup>14</sup>, John Kiiru<sup>6</sup>, Catherine Kyobutungi<sup>15</sup>, Cecila Tacoli<sup>16</sup>, Erastus Kang’ethe<sup>5</sup>, Julio Davila<sup>17</sup>, Sam Kariuki<sup>6</sup>, Timothy Robinson<sup>18</sup>, Jonathan Rushton7</sup>, Mark E. J. Woolhouse<sup>1,3</sup>, Eric Fèvre<sup>2,7</sup>

1. Usher Institute, University of Edinburgh, Edinburgh, United Kingdom
2. International Livestock Research Institute, Nairobi, Kenya
3. Centre for Immunity, Infection and Evolution, University of Edinburgh, Edinburgh, United Kingdom
4. Nuﬃeld Department of Clinical Medicine, University of Oxford, John Radcliﬀe Hospital, Oxford, United Kingdom
5. University of Nairobi, Nairobi, Kenya
6. Kenya Medical Research Institute, Nairobi, Kenya
7. Institute of Infection, Veterinary and Ecological Sciences, University of Liverpool, Neston, United Kingdom
8. National Museums of Kenya, Nairobi, Kenya
9. Research Center for Zoonosis Control, Hokkaido University, Japan
10. Department of Epidemiology, Columbia University, New York, United States
11. Veterinary Services Department, Kenya Wildlife Service, Kenya
12. Uppsala University, Uppsala, Sweden
13. Big Data Institute, Nuﬃeld Department of Population Health, University of Oxford, Oxford, United Kingdom
14. Natural Resources Institute, University of Greenwich, Chatham Maritime, United Kingdom
15. African Population Health Research Centre, Nairobi, Kenya
16. International Institute for Environment and Development, London, United Kingdom
17. The Bartlett Development Planning Unit, Faculty of the Built Environment, University College London, London, United Kingdom
18. Food and Agriculture Organization of the United Nations, Rome, Italy

\* These authors contributed equally


## Methodology
Human, animal and wildlife faecal samples were collected and transported on ice to one of two laboratories (University of Nairobi or Kenya Medical Research Institute) within 5 h of collection. Samples were enriched in buffered peptone water for 24 h, and thereafter plated on to eosin methylene blue agar (EMBA) and incubated for 24 h at 37 °C. Subsequently, five colonies were selected and subcultured on EMBA, before being further subcultured on Müller-Hinton agar. A single colony was picked at random from the plate for each original sample (hereafter referred to as an ‘isolate’) and a 10 parameter biochemical test was used (triple sugar iron agar=4, Simmon's citrate agar=1, and motility-indole-lysine media=3, urease production from urea media =1, oxidase from tetra-methyl- p-phenylenediamine dihydrochloride = 1) were used for presumptive identification of *E coli*. 

## References

1. 

## Author's Contact

Dr Deirdre McClean <deirdre.mcclean@ed.ac.uk>
